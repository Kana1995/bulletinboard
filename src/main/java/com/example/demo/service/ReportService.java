package com.example.demo.service;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entity.Report;
import com.example.demo.repository.ReportRepository;

@Service
public class ReportService {

	@Autowired
	ReportRepository reportRepository;

	// レコード全件取得
	public List<Report> findAllReport(String startDate, String endDate) throws ParseException {

		// 表示形式を指定
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date startDateData = new Date();
		Date endDateData = new Date();

		//開始日時を用意
        if(startDate != null){
			startDateData = dateFormat.parse(startDate + " 00:00:00") ;
        } else {
        	startDateData = dateFormat.parse("2020-01-01 00:00:00");
        }
        //終了日時を用意
        if(endDate != null ) {
        	endDateData = dateFormat.parse(endDate + " 23:59:59" );
        } else {
        	// 現在日時を取得
    		Date date = new Date();
    		endDateData = date;
        }

        System.out.println(startDate);
        System.out.println(endDate);
		return reportRepository.findByCreatedDateBetween(startDateData, endDateData);
	}

	// レコード追加
	public void saveReport(Report report) {
	reportRepository.save(report);
	}

	//レコードの削除
	public void deleteReport(Integer id) {
		reportRepository.deleteById(id);
	}

	//レコード1件取得
	public Report editReport(Integer id) {
	Report report = (Report) reportRepository.findById(id).orElse(null);
	return report;
	}
}
