package com.example.demo.entity;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "comment")

public class Comment {
	@Id
	@Column

	//テーブルのidentity列を利用して，主キー値を生成
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Column
	private String comment;

	@Column
	private int reportId;

	@Column(name = "created_date")
	@DateTimeFormat(pattern="yyyy-MM-dd")
	private Date createdDate;

	@Column(name = "updated_date")
	@DateTimeFormat(pattern="yyyy-MM-dd")
	private Date updatedDate;

	public int getId() {
	return id;
	}

	public void setId(int id) {
	this.id = id;
	}

	public String getComment() {
	return comment;
	}

	public void setComment(String comment) {
	this.comment = comment;
	}

	public int getReportId() {
		return reportId;
	}

	public void setReportId(int reportId) {
		this.reportId = reportId;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
}