package com.example.demo.controller;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.entity.Comment;
import com.example.demo.entity.Report;
import com.example.demo.service.CommentService;
import com.example.demo.service.ReportService;



@Controller
public class ForumController {

	@Autowired
	ReportService reportService;

	@Autowired
	CommentService commentService;

	// 投稿内容表⽰画⾯
	@GetMapping
	public ModelAndView top(@RequestParam(name="startDate", required = false) String startDate
			,@RequestParam(name="endDate",required = false) String endDate) throws ParseException {
		ModelAndView mav = new ModelAndView();


		// 開始日時と終了日時を指定して一覧取得
		List<Report> contentData = reportService.findAllReport(startDate, endDate);

		// コメント投稿を全件取得
		List<Comment> contentCommentData = commentService.findAllComment();

		// 画⾯遷移先を指定(RequestMapping)
		mav.setViewName("/top");

		// 投稿データオブジェクトを保管
		mav.addObject("contents", contentData);
		// コメントデータオブジェクトを保管
		mav.addObject("contentComments", contentCommentData);

		mav.addObject("startDate", startDate);
		mav.addObject("endDate", endDate);

		return mav;
		}

	// 新規投稿画面
	@GetMapping("/new")
	public ModelAndView newContent() {
		ModelAndView mav = new ModelAndView();

		// form⽤の空のentityを準備
		Report report = new Report();
		// 画⾯遷移先を指定
		mav.setViewName("/new");
		// 準備した空のentityを保管
		mav.addObject("formModel", report);
		return mav;
		}

	// 投稿処理
	@PostMapping("/add")
	public ModelAndView addContent(@ModelAttribute("formModel") Report report) {

		Date date = new Date();
		report.setCreatedDate(date);
		report.setUpdatedDate(date);

		// 投稿をテーブルに格納
		reportService.saveReport(report);
		// rootへリダイレクト
		return new ModelAndView("redirect:/");
		}

	//投稿削除
	@DeleteMapping("/delete/{id}")
		public ModelAndView deleteContent(@PathVariable Integer id) {
			reportService.deleteReport(id);
			return new ModelAndView("redirect:/"); //rootへリダイレクト
		}

	// 更新画面
	@GetMapping("/edit/{id}")
	public ModelAndView editContent(@PathVariable Integer id) {
		ModelAndView mav = new ModelAndView();
		// 編集する投稿を取得
		Report report = reportService.editReport(id);
		// 編集する投稿をセット
		mav.addObject("formModel", report);
		// 画面遷移先を指定
		mav.setViewName("/edit");
		return mav;
		}

	// 更新処理
	@PutMapping("/update/{id}")
	public ModelAndView updateContent (@PathVariable Integer id, @ModelAttribute("formModel") Report report) {
		// UrlParameterのidを更新するentityにセット
		report.setId(id);
		// 編集した投稿を更新
		reportService.saveReport(report);
		// rootへリダイレクト
		return new ModelAndView("redirect:/");
		}

}
