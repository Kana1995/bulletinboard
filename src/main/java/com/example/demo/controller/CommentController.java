package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.entity.Comment;
import com.example.demo.service.CommentService;

@Controller
public class CommentController {

	@Autowired
	CommentService commentService;

	// コメント新規投稿画面
	@GetMapping("/newComment")
	public ModelAndView newContentComment(@RequestParam(name="reportId") int reportId) {
		ModelAndView mav = new ModelAndView();

		// コメント用の空のentityを準備
		Comment comment = new Comment();
		// UrlParameterのidを更新するentityにセット
		comment.setReportId(reportId);
		// 画⾯遷移先を指定
		mav.setViewName("/newComment");
		// 準備した空のentityを保管
		mav.addObject("commentModel", comment);
		return mav;
		}

	// コメント投稿処理
	@PostMapping("/addComment")
	public ModelAndView addComment(@ModelAttribute("commentModel")  Comment comment){
		// コメントをテーブルに格納
		commentService.saveComment(comment);
		// rootへリダイレクト
		return new ModelAndView("redirect:/");
		}

	// コメント更新画面
	@GetMapping("/editComment/{id}")
	public ModelAndView editComment(@PathVariable Integer id) {
		ModelAndView mav = new ModelAndView();
		// 編集する投稿を取得
		Comment comment = commentService.editComment(id);
		//comment.setReportId(reportId);
		// 編集する投稿をセット
		mav.addObject("commentModel", comment);
		// 画面遷移先を指定
		mav.setViewName("/editComment");
		return mav;
		}

	// コメント更新処理
	@PutMapping("/updateComment/{id}")
	public ModelAndView updateComment (@PathVariable Integer id, @ModelAttribute("commentModel") Comment comment,@RequestParam(name="reportId") Integer reportId) {
		// UrlParameterのidを更新するentityにセット
		comment.setId(id);
		// UrlParameterのidを更新するentityにセット
		comment.setReportId(reportId);
		// 編集した投稿を更新
		commentService.saveComment(comment);
		// rootへリダイレクト
		return new ModelAndView("redirect:/");
		}

	//コメント削除
	@DeleteMapping("/deleteComment/{id}")
		public ModelAndView deleteComment(@PathVariable Integer id) {
			commentService.deleteComment(id);
			return new ModelAndView("redirect:/"); //rootへリダイレクト
		}

}
